import argparse
from typing import TextIO


class Cave:
    def __init__(self, name):
        self.name = name
        self.connected_caves = []
        self.visit_count = 0

    def get_name(self):
        return self.name

    def is_big(self):
        return self.name.isupper()

    def add_connection(self, other):
        self.connected_caves.append(other)

    def get_connections(self):
        return self.connected_caves

    def __repr__(self):
        return self.name


class System:
    def __init__(self, text):
        self.caves = {'start': Cave('start'), 'end': Cave('end')}

        for line in text.split('\n'):
            cave_1, cave_2 = line.split('-')
            if cave_1 not in self.caves:
                self.caves[cave_1] = Cave(cave_1)
            if cave_2 not in self.caves:
                self.caves[cave_2] = Cave(cave_2)
            cave_1 = self.caves[cave_1]
            cave_2 = self.caves[cave_2]
            cave_1.add_connection(cave_2)
            cave_2.add_connection(cave_1)

    def get_route_count(self):
        start = self.caves['start']
        return self._route_count_helper(start, [start])

    def _route_count_helper(self, current_cave: Cave, path):
        if current_cave is self.caves['end']:
            return 1
        routes = 0
        for cave in current_cave.get_connections():
            if cave.is_big() or cave not in path:
                routes += self._route_count_helper(cave, path + [cave])
        return routes


class System2:
    def __init__(self, text):
        self.caves = {'start': Cave('start'), 'end': Cave('end')}

        for line in text.split('\n'):
            cave_1, cave_2 = line.split('-')
            if cave_1 not in self.caves:
                self.caves[cave_1] = Cave(cave_1)
            if cave_2 not in self.caves:
                self.caves[cave_2] = Cave(cave_2)
            cave_1 = self.caves[cave_1]
            cave_2 = self.caves[cave_2]
            cave_1.add_connection(cave_2)
            cave_2.add_connection(cave_1)

    def get_route_count(self):
        start = self.caves['start']
        return self._route_count_helper(start, [start], None)

    def _route_count_helper(self, current_cave: Cave, path, double):
        if current_cave is self.caves['end']:
            return 1
        routes = 0
        for cave in current_cave.get_connections():
            if cave.is_big() or cave not in path:
                routes += self._route_count_helper(cave, path + [cave], double)
            elif double is None and cave is not self.caves['start'] and path.count(cave) < 2:
                routes += self._route_count_helper(cave, path + [cave], cave)
        return routes


def part_1(input_file: TextIO):
    system = System(input_file.read())
    return system.get_route_count()


def part_2(input_file: TextIO):
    system = System2(input_file.read())
    return system.get_route_count()


def main():
    parser = argparse.ArgumentParser(description='Advent of Code Day 12')
    part_group = parser.add_mutually_exclusive_group(required=True)
    part_group.add_argument('--part-1', action='store_true', help='Run Part 1')
    part_group.add_argument('--part-2', action='store_true', help='Run Part 2')
    parser.add_argument('--example', action='store_true', help='Run part with example data')
    args = parser.parse_args()

    part_number = '1' if args.part_1 else '2'
    function = part_1 if args.part_1 else part_2
    example = '_example' if args.example else ''
    input_file_path = f'input/part_{part_number}{example}.txt'

    with open(input_file_path, 'r') as input_file:
        print(function(input_file))


if __name__ == '__main__':
    main()
